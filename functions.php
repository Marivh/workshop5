<?php

function getConnection() {
    $connection = new mysqli('localhost:3306', 'root', '', 'students');
    if ($connection->connect_errno) {
      printf("Connect failed: %s\n", $connection->connect_error);
      die;
    }
    return $connection;
  }
?>